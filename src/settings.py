from typing import Tuple

import torch


class Config:
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    batch_size: int = 15
    size: Tuple[int, int] = (256, 256)


proj_conf = Config()
