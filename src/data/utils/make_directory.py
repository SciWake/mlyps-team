import os.path

import numpy as np
from skimage.io import imsave

from src.data.utils.preprocess import ImageType


def check_exists_dir(dir: str) -> bool:
    """Check existing of dir and
        create dir if not exist

    Args:
        dir (str): path to directory

    Returns:
        bool: Is existed before using the function
    """
    if not os.path.exists(dir):
        os.mkdir(path=dir)
        return False

    return True


def load_images(path: str, images: ImageType,
                lesions: ImageType, indices: np.ndarray):
    """Save images and masks to processed dir

    Args:
        path (str): path to processed dir
        images (ImageType): tuple of np.array with images and path to images
        lesions (ImageType): tuple of np.array with masks and path to masks
        indices (np.ndarray): shuffled indices of separated data
    """
    if not check_exists_dir(path):
        check_exists_dir(os.path.join(path, "images"))
        check_exists_dir(os.path.join(path, "lesions"))

        for idx in indices:
            imsave(os.path.join(path, "images/", images[idx][1]), images[idx][0])
            imsave(os.path.join(path, "lesions/", lesions[idx][1]), lesions[idx][0])
