import os

from typing import Tuple, TypeAlias, List

import numpy as np

from src.data.utils.make_directory import check_exists_dir, load_images


ImageType: TypeAlias = List[Tuple[np.array, str]]


def unpack(path: str):
    """Unpack data from rar to directory

    Args:
        path (str): path to rar file
    """
    import patoolib

    full_path = os.path.join(path, "PH2Dataset")

    if not check_exists_dir(full_path):
        patoolib.extract_archive(full_path + ".rar", outdir=path)


def images_crawler(path: str) -> Tuple[ImageType, ImageType]:
    """Crawle all images from raw data

    Args:
        path (str): path to images store

    Returns:
        Tuple: tuple of images and masks np.array
    """
    from skimage.io import imread

    images, lesions = [], []

    for root, dirs, files in os.walk(
        os.path.join(path, "PH2Dataset/PH2 Dataset images")
    ):
        if root.endswith("_Dermoscopic_Image"):
            res = (imread(os.path.join(root, files[0])), files[0])
            images.append(res)

        if root.endswith("_lesion"):
            res = (imread(os.path.join(root, files[0])), files[0])
            lesions.append(res)

    return images, lesions


def decompose_data(path: str, images: ImageType,
                   lesions: ImageType):
    """Separating data on train, valid, test
        and save to processed dir

    Args:
        path (str): path to processed dir
        images (Tuple): tuple of np.array with images and path to images
        lesions (Tuple): tuple of np.array with masks and path to masks
    """
    import numpy as np

    ix = np.random.choice(a=len(images), size=len(images), replace=False)
    tr, val, ts = np.split(ix, [100, 150])

    train_path = os.path.join(path, "train")
    test_path = os.path.join(path, "test")
    val_path = os.path.join(path, "valid")

    load_images(path=train_path, images=images, lesions=lesions, indices=tr)
    load_images(path=val_path, images=images, lesions=lesions, indices=val)
    load_images(path=test_path, images=images, lesions=lesions, indices=ts)
