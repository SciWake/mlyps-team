# -*- coding: utf-8 -*-
import click
import logging


@click.group(name="process_data")
def group():
    ...


@group.command(name="run")
@click.option("--input_filepath", type=click.Path(exists=True))
@click.option("--output_filepath", type=click.Path())
def run(input_filepath: str, output_filepath: str):
    """Runs data processing scripts to turn raw data into
        cleaned data ready to be analyzed

    Args:
        input_filepath (str): raw data path
        output_filepath (str): processed data path
    """

    from src.data.utils.preprocess import unpack, decompose_data, images_crawler

    logger = logging.getLogger(__name__)
    logger.info("making final data set from raw data")

    unpack(input_filepath)
    # We can create this process without crawling.
    # Just add copy images from raw dir to processed dir
    images, lesions = images_crawler(input_filepath)
    decompose_data(output_filepath, images, lesions)
    logger.info("Data was cleaned")
