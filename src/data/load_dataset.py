import os.path
import numpy as np
from typing import Tuple
from skimage.io import imread
from skimage.transform import resize
from torch.utils.data import DataLoader
from src.settings import proj_conf


def _load(path: str) -> Tuple[np.array, np.array]:
    x = sorted(os.listdir(os.path.join(path, "images/")))
    y = sorted(os.listdir(os.path.join(path, "lesions/")))

    images, lesions = [], []

    for image_path, lesion_path in zip(x, y):
        images.append(imread(os.path.join(path, "images/", image_path)))
        lesions.append(imread(os.path.join(path, "lesions/", lesion_path)))

    images = [
        resize(
            x,
            proj_conf.size,
            mode="constant",
            anti_aliasing=True,
        )
        for x in images
    ]
    lesions = [
        resize(y, proj_conf.size, mode="constant", anti_aliasing=False) > 0.5
        for y in lesions
    ]

    images = np.array(images, np.float32)
    lesions = np.array(lesions, np.float32)

    return images, lesions


def load_train(path: str) -> Tuple[DataLoader, DataLoader]:
    """Function for loading images from directories to DataLoaders

    Args:
        path (str): path to processed data

    Returns:
        Tuple[DataLoader, DataLoader]: tuple of train and valid DataLoaders
    """
    train_path = os.path.join(path, "train/")
    valid_path = os.path.join(path, "valid/")

    images_tr, lesions_tr = _load(train_path)
    images_val, lesions_val = _load(valid_path)

    data_tr = DataLoader(
        list(zip(np.rollaxis(images_tr, 3, 1), lesions_tr[:, None])),
        batch_size=proj_conf.batch_size,
        shuffle=True,
    )
    data_val = DataLoader(
        list(zip(np.rollaxis(images_val, 3, 1), lesions_val[:, None])),
        batch_size=proj_conf.batch_size,
        shuffle=True,
    )

    return data_tr, data_val
