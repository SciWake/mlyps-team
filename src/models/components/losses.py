import torch
from typing import TypeAlias, Callable

LossType: TypeAlias = Callable[[torch.Tensor, torch.Tensor], torch.Tensor]


def bce_loss(y_real: torch.Tensor, y_pred: torch.Tensor) -> torch.Tensor:
    """Creates a criterion that measures
        the Binary Cross Entropy between the target and the input probabilities

    Args:
        y_real (torch.Tensor): real masks
        y_pred (torch.Tensor): predicted masks

    Returns:
        torch.Tensor: loss between real and predicted masks
    """
    return torch.mean(y_pred - y_real * y_pred + torch.log(1 + torch.exp(-y_pred)))
