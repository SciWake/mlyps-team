import torch
from typing import TypeAlias, Callable


MetricType: TypeAlias = Callable[[torch.Tensor, torch.Tensor], torch.Tensor]


def iou_pytorch(outputs: torch.Tensor, labels: torch.Tensor) -> torch.Tensor:
    """Calculates Intersection over Union

    Args:
        outputs (torch.Tensor): predicted masks
        labels (torch.Tensor): real masks

    Returns:
        torch.Tensor: thresholded iou metric
    """
    outputs = outputs.squeeze(1).byte()  # BATCH x 1 x H x W => BATCH x H x W
    labels = labels.squeeze(1).byte()
    SMOOTH = 1e-8
    intersection = (
        (outputs & labels).float().sum((1, 2))
    )  # Will be zero if Truth=0 or Prediction=0
    union = (outputs | labels).float().sum((1, 2))  # Will be zero if both are 0

    iou = (intersection + SMOOTH) / (
        union + SMOOTH
    )  # We smooth our devision to avoid 0/0

    thresholded = (
        torch.clamp(20 * (iou - 0.5), 0, 10).ceil() / 10
    )  # This is equal to comparing with thresolds

    return thresholded
