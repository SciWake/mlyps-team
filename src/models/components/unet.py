import torch
import torch.nn as nn
from src.models.components.layers import Custom2LayersCNN, Custom3LayersCNN


class UNet2(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        """UNet 2.0 is a modified version of UNet
             for better segmentation even image is noisy
        """

        # encoder
        self.enc_conv0 = Custom2LayersCNN(
            in_channels=3, out_channels=32, kernel_size=3, stride=1, padding=1
        )
        self.pool0 = torch.nn.Conv2d(
            32, 32, kernel_size=2, stride=2, padding=0
        )  # 256 -> 128
        self.enc_conv1 = Custom2LayersCNN(
            in_channels=32, out_channels=64, kernel_size=3, stride=1, padding=1
        )
        self.pool1 = torch.nn.Conv2d(
            64, 64, kernel_size=2, stride=2, padding=0
        )  # 128 -> 64
        self.enc_conv2 = Custom3LayersCNN(
            in_channels=64, out_channels=128, kernel_size=3, stride=1, padding=1
        )
        self.pool2 = torch.nn.Conv2d(
            128, 128, kernel_size=2, stride=2, padding=0
        )  # 64 -> 32
        self.enc_conv3 = Custom3LayersCNN(
            in_channels=128, out_channels=256, kernel_size=3, stride=1, padding=1
        )
        self.pool3 = torch.nn.Conv2d(
            256, 256, kernel_size=2, stride=2, padding=0
        )  # 32 -> 16

        # bottleneck
        self.bottleneck_conv = nn.Sequential(
            nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
        )

        # decoder
        self.upsample0 = torch.nn.ConvTranspose2d(
            256 * 2, 256 * 2, kernel_size=2, stride=2
        )  # 16 -> 32
        self.dec_conv0 = Custom3LayersCNN(
            in_channels=256 * 2, out_channels=128, kernel_size=3, stride=1, padding=1
        )
        self.upsample1 = torch.nn.ConvTranspose2d(
            128 * 2, 128 * 2, kernel_size=2, stride=2
        )  # 32 -> 64
        self.dec_conv1 = Custom3LayersCNN(
            in_channels=128 * 2, out_channels=64, kernel_size=3, stride=1, padding=1
        )
        self.upsample2 = torch.nn.ConvTranspose2d(
            64 * 2, 64 * 2, kernel_size=2, stride=2
        )  # 64 -> 128
        self.dec_conv2 = Custom3LayersCNN(
            in_channels=64 * 2, out_channels=32, kernel_size=3, stride=1, padding=1
        )
        self.upsample3 = torch.nn.ConvTranspose2d(
            32 * 2, 32 * 2, kernel_size=2, stride=2
        )  # 128 -> 256
        self.dec_conv3 = Custom3LayersCNN(
            in_channels=32 * 2, out_channels=1, kernel_size=3, stride=1, padding=1
        )

    def forward(self, x):
        # encoder
        e0 = self.pool0(self.enc_conv0(x))
        e1 = self.pool1(self.enc_conv1(e0))
        e2 = self.pool2(self.enc_conv2(e1))
        e3 = self.pool3(self.enc_conv3(e2))
        # bottleneck
        b = self.bottleneck_conv(e3)

        # decoder
        d0 = self.dec_conv0(self.upsample0(torch.cat([e3, b], dim=1)))
        d1 = self.dec_conv1(self.upsample1(torch.cat([e2, d0], dim=1)))
        d2 = self.dec_conv2(self.upsample2(torch.cat([e1, d1], dim=1)))
        d3 = self.dec_conv3(self.upsample3(torch.cat([e0, d2], dim=1)))
        return d3
