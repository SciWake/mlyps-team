import torch
import torch.nn as nn


class CustomConvLayer(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: int,
        stride: int = 1,
        padding: int = 1,
        inplace: bool = False,
    ):
        """CustomConvLayer is sequence of nn.Conv2,
            nn.BatchNorm2d and nn.ReLU

        Args:
            in_channels (int): input size
            out_channels (int): output size
            kernel_size (int): size of filter
            stride (int, optional): Defaults to 1.
            padding (int, optional): Defaults to 1.
            inplace (bool, optional): Defaults to Fals
        """
        super(CustomConvLayer, self).__init__()
        self.layer = nn.Sequential(
            nn.Conv2d(
                in_channels,
                out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
            ),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=inplace),
        )

    def forward(self, inp: torch.Tensor):
        output = self.layer(inp)
        return output


class Custom2LayersCNN(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: int,
        stride: int = 1,
        padding: int = 1,
        inplace: bool = False,
    ):
        """There are two CustomConvLayer models

        Args:
            in_channels (int): input size
            out_channels (int): output size
            kernel_size (int): size of filter
            stride (int, optional): Defaults to 1.
            padding (int, optional): Defaults to 1.
            inplace (bool, optional): Defaults to False.
        """
        super(Custom2LayersCNN, self).__init__()
        self.layer = nn.Sequential(
            CustomConvLayer(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                inplace=inplace,
            ),
            CustomConvLayer(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                inplace=inplace,
            ),
        )

    def forward(self, inp: torch.Tensor):
        output = self.layer(inp)
        return output


class Custom3LayersCNN(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: int,
        stride: int = 1,
        padding: int = 1,
        inplace: bool = False,
    ):
        super(Custom3LayersCNN, self).__init__()
        self.layer = nn.Sequential(
            CustomConvLayer(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                inplace=inplace,
            ),
            CustomConvLayer(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                inplace=inplace,
            ),
            CustomConvLayer(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=stride,
                padding=padding,
                inplace=inplace,
            ),
        )

    def forward(self, inp: torch.Tensor):
        output = self.layer(inp)
        return output
