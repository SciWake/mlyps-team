import click
import torch
import torch.optim as optim
import torch.nn as nn
from src.models.train_model import train
from src.models.components.unet import UNet2
from src.settings import proj_conf
from src.models.components.losses import bce_loss
from src.data.load_dataset import load_train


@click.group(name="models")
def group():
    ...


@group.command(name="train")
@click.option("--input_filepath", type=click.Path(exists=True))
@click.option("--output_filepath", type=click.Path(exists=True))
def train_runner(input_filepath: str, output_filepath: str):
    """
        python3 manager.py models train --input_filepath "data/processed/"

    Args:
        input_filepath (str): path to train and valid data
        output_filepath(str): path for saving trained model
    """
    model: nn.Module = UNet2().to(proj_conf.device)
    data_tr, data_val = load_train(input_filepath)

    max_epochs = 1
    opt = optim.AdamW(model.parameters(), lr=1e-4)
    scheduler = optim.lr_scheduler.StepLR(opt, step_size=10, gamma=0.5)
    model = train(
        model,
        opt,
        bce_loss,
        max_epochs,
        data_tr,
        data_val,
        scheduler=scheduler
    )

    torch.onnx.export(model, output_filepath, verbose=True)


@group.command(name="predict")
@click.option("--input_filepath", type=click.Path(exists=True))
def predict_runner(input_filepath: str):
    """
        python3 manager.py models predict --input_filepath "data/processed/"

    Args:
        input_filepath (str): path to test data
    """
    pass
