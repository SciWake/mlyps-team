import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from tqdm import tqdm
from typing import Tuple

from src.settings import proj_conf
from src.models.components.metrics import iou_pytorch
from src.models.score_model import score_model, ScoreType
from src.models.components.losses import LossType
from src.models.components.metrics import MetricType


def _train_batch(
    model: nn.Module,
    x_batch: torch.Tensor,
    y_batch: torch.Tensor,
    opt: torch.optim,
    loss_fn: LossType,
    metric: MetricType = iou_pytorch,
    scheduler: torch.optim.lr_scheduler = None,
) -> Tuple[torch.Tensor, torch.Tensor]:
    """The function for training model on batch of data

    Args:
        model (nn.Module): customed segmentaion model based on nn.Module
        x_batch torch.Tensor: images
        y_batch torch.Tensor: masks
        opt (torch.optim): optimizer
        loss_fn (LossType): there can be any loss function
        metric (MetricType, optional): Defaults to iou_pytorch.
        scheduler (torch.optim.lr_scheduler, optional): Defaults to None.

    Returns:
        Tuple: with loss and metric
    """
    x_batch = x_batch.to(proj_conf.device)
    y_batch = y_batch.to(proj_conf.device)

    opt.zero_grad()
    y_pred = model(x_batch)
    iou = metric(torch.sigmoid(y_pred) > 0.5, y_batch).mean().item()
    loss = loss_fn(y_batch, y_pred)
    loss.backward()
    opt.step()
    if scheduler:
        scheduler.step()

    return loss, iou


def train(
    model: nn.Module,
    opt: torch.optim,
    loss_fn: LossType,
    epochs: int,
    data_tr: DataLoader,
    data_val: DataLoader,
    scheduler: torch.optim.lr_scheduler = None,
    metric: MetricType = iou_pytorch,
    score_model: ScoreType = score_model,
) -> nn.Module:
    """Train model on train_data and val_data

    Args:
        model (nn.Module): customed segmentaion model
        opt (torch.optim): optimizer
        loss_fn (LossType): Any loss function
        epochs (int): Number of epochs
        data_tr (DataLoader): Dataloader with images and masks
        data_val (DataLoader): Dataloader with images and masks
        scheduler (torch.optim.lr_scheduler, optional):  Defaults to None.
        metric (MetricType, optional): Defaults to iou_pytorch.
        score_model (ScoreType, optional): score our precisions.
    """

    history = []
    for epoch in range(epochs):
        with tqdm(data_tr, unit="batch") as tepoch:
            tepoch.set_description(f"Epoch {epoch + 1}")

            log_template = (
                "\nEpoch {ep:03d} train_loss: {t_loss:0.4f} train_iou: "
                "{t_iou:0.4f} val_loss: {v_loss:0.4f} val_iou: {v_iou:0.4f}"
            )
            avg_iou_train, avg_loss_train, avg_loss_val = 0, 0, 0

            model.train()  # train mode
            for x_batch, y_batch in tepoch:
                loss, iou = _train_batch(
                    model=model,
                    x_batch=x_batch,
                    y_batch=y_batch,
                    opt=opt,
                    loss_fn=loss_fn,
                    metric=iou_pytorch,
                    scheduler=scheduler,
                )

                avg_loss_train += loss / len(data_tr)
                avg_iou_train += iou / len(data_tr)

            # show intermediate results
            model.eval()
            iou_val = score_model(model, metric, data_val)
            # validation loss
            for x_batch, y_batch in data_val:
                with torch.set_grad_enabled(False):
                    y_pred = model(x_batch)
                avg_loss_val += loss_fn(y_batch, y_pred) / len(data_val)

            history.append((avg_loss_train, avg_iou_train, avg_loss_val, iou_val))
            # Visualize tools

            tepoch.update(1)
            tqdm.write(
                log_template.format(
                    ep=epoch + 1,
                    t_loss=avg_loss_train,
                    t_iou=avg_iou_train,
                    v_loss=avg_loss_val,
                    v_iou=iou_val,
                )
            )
            # torch.cuda.empty_cache()

    return model
