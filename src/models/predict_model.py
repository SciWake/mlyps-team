import numpy as np
import torch.nn as nn
from src.settings import proj_conf


def predict(model: nn.Module, data: np.array) -> np.array:
    """Eval your images to masks

    Args:
        model (nn.Module): trained segmentation model based on nn.Module
        data (np.array): test images and true test masks
    Returns:
        np.array: predicted masks
    """

    model.eval()
    y_pred = [
        model(x_batch.to(proj_conf.device)).detach().cpu().numpy()
        for x_batch, _ in data
    ]
    return np.array(y_pred)
