import torch
import numpy as np
import torch.nn as nn
from typing import TypeAlias
from collections.abc import Callable
from src.settings import proj_conf
from src.models.components.metrics import MetricType


ScoreType: TypeAlias = Callable[[nn.Module, MetricType, np.array], float]


def score_model(model: nn.Module,
                metric: MetricType,
                data: np.array) -> float:
    """Count the score of predictions and divide by the number of data

    Args:
        model (nn.Module): customed segmentaion model based on nn.Module
        metric (Callable): evaluation function
        data (np.array): batch of images and masks

    Returns:
        float: operation score
    """
    model.eval()
    scores = 0
    for x_batch, y_label in data:
        print("device", proj_conf.device.type)
        y_pred = torch.sigmoid(model(x_batch.to(proj_conf.device))) > 0.5
        scores += metric(y_pred, y_label.to(proj_conf.device)).mean().item()
    return scores / len(data)
