import click


from src.data import make_dataset as data_cli
from src.models import cli as models_cli


@click.group()
def entire_group():
    ...


if __name__ == "__main__":
    entire_group.add_command(data_cli.group)
    entire_group.add_command(models_cli.group)
    entire_group()
